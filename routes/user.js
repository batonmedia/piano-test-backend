const express = require('express');
const router = express.Router();
const passport = require('passport');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const User = require('../models/user');
const BCRYPT_SALT_ROUNDS = 12;

router.get('/',
	passport.authenticate('bearer', { session: false }),
	(req, res, next) => {
		User.findOne().byToken(req.user.token).exec().then(model => {
			const { first_name, username } = model;
			res.json({
				first_name,
				username,
			})
		}).catch(err => {
			console.log(err);
			next();
		});
	});

router.post('/sign-up', (req, res, next) => {
	const { first_name, username, password } = req.body;
	bcrypt.hash(password, BCRYPT_SALT_ROUNDS).then(hashedPassword => {
		User.create({
			first_name,
			username,
			password: hashedPassword,
		}).then(model => {
			res.status(201).send('OK');
		}).catch(err => {
			console.log(err);
			res.status(400).send('Already exist');
		});
	})
	.catch(error => {
		console.log();
		console.log('Error creating user: ', error);
		next();
	});
});

router.post('/sign-in', (req, res, next) => {
	const { username, password } = req.body;
	User.findOne().byUsername(username).exec().then(model => {
		bcrypt.compare(password, model.password).then(samePassword => {
			if (samePassword) {
				model.token = jwt.sign({username: model.username}, 'secret', {expiresIn: '1h'});
				model.save().then(updatedModel => {
					console.log(updatedModel);
					res.json({
						token: updatedModel.token,
					});
				}).catch(err => {
					console.log(err);
					next();
				})
			} else {
				res.status(401).send('Wrong password');
			}
		});
	}).catch(err => {
		res.status(404).send('No user with provided username');
	});
});

module.exports = router;
