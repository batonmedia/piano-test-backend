const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
	id: Number,
	first_name: {
		type: String,
		required: true,
	},
	username: {
		type: String,
		unique: true,
		required: true,
		dropDups: true,
	},
	password: {
		type: String,
		required: true
	},
	token: {
		type: String,
		unique: true,
	},
});

UserSchema.query.byUsername = function (username) {
	return this.where({ username: username });
};

UserSchema.query.byToken = function (token) {
	return this.where({ token: token });
};

module.exports = mongoose.model('user', UserSchema );
