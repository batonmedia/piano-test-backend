# Piano Test Backend

## Requirements
- [MongoDB](https://www.mongodb.com/) installed
- [Node.js](https://nodejs.org) v10+
- [Yarn](https://yarnpkg.com) package manager (of course you can use NPM, but... you know ;)

## Setup the app
1. Create new Mongo database the way you prefer;
2. Install project dependencies with `yarn install` command;
3. Edit `environment.js` accordingly to your MongoDB host & database name;
4. Run `yarn start` or `npm run start` to start the app.

The backend will be available on `http://localhost:3000`.
