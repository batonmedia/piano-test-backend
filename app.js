const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');
const passport = require('passport');
const BearerStrategy = require('passport-http-bearer').Strategy;
const mongoose = require('mongoose');
const cors = require('cors');

const environment = require('./environment');

const User = require('./models/user');

passport.use('bearer', new BearerStrategy((token, done) => {
	User.findOne().byToken(token).exec().then(model => {
		done(null, model, { scope: 'all' });
	}).catch(err => {
		return done(err);
	});
}));

const indexRouter = require('./routes/index');
const userRouter = require('./routes/user');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors());

app.use('/', indexRouter);
app.use('/user', userRouter);


mongoose.connect(`mongodb://${environment.databaseHost}/${environment.databaseName}`, {
	useNewUrlParser: true
});
mongoose.set('useCreateIndex', true);
mongoose.Promise = global.Promise;
const dbConnection = mongoose.connection;
dbConnection.on('connected', () => {
	console.log('Succesfully connected to database');
});

module.exports = app;
